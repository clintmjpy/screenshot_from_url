"""This Program takes a Sceen Shot of a Live URL webpage
Copyright (C) 2020  -- Author : Clint Johnson

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA."""


from selenium import webdriver
#from webdriver_manager.firefox import GeckoDriverManager
from selenium.webdriver.chrome.options import Options
from time import sleep
import click
import subprocess
import tldextract

# 1 Download Chrome Driver https://sites.google.com/a/chromium.org/chromedriver/downloads
# wget https://chromedriver.storage.googleapis.com/86.0.4240.22/chromedriver_linux64.zip
# Unzip Chrome Driver
# Get realpath for chromedriver
# Options Help : https://askubuntu.com/questions/31069/how-to-open-a-file-manager-of-the-current-directory-in-the-terminal


@click.command()
@click.option('--urltograb', prompt='URL to Screen Shot',help='Insert the URL to Screen Shot Here')
@click.option('--toporfull', prompt='Full Screen or Top Only?',help='Top to Grab only top, full to Grab Whole Page')
def downloadscreenshot(urltograb,toporfull):
    """This Program downloads a Screen Shot of a WebPage by URL"""
    
    filename = tldextract.extract(urltograb).domain    
    chrome_options = Options()
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--start-maximized')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-dev-shm-usage')

    driver = webdriver.Chrome(executable_path='/home/shane/chromedriver',options=chrome_options)
    
    #Another Driver option that could not get to work.
    #driver = webdriver.Firefox(executable_path=GeckoDriverManager().install())
    
    driver.get(urltograb)
    sleep(1)

    if toporfull.lower() == 'top': # Download content above the fold.
        driver.get_screenshot_as_file(f"{filename}_top.png")
    elif toporfull.lower() == 'full': # Download entire Page Top to Bottom.
        S = lambda X: driver.execute_script('return document.body.parentNode.scroll'+X)
        driver.set_window_size(S('Width'),S('Height')) # May need manual adjustment
        driver.find_element_by_tag_name('body').screenshot(f"{filename}_full.png")

    sleep(1) #Might be able to lower this amount, or get rid of it.
    
    driver.quit()
    # Open Save to Directory after save is Complete.
    subprocess.call(f"xdg-open .",shell=True)
    print(f"Took Screen Shot of {urltograb}, have a nice day!")

if __name__ == "__main__":
    downloadscreenshot()
